from setuptools import setup, find_packages

setup(
    name='dssl',
    version='2020.04.27',
    description='Disease severity score learning',
    url='https://bitbucket.org/atpage/dsslpy/',
    author='Alex Page',
    author_email='alex.page@rochester.edu',
    classifiers=[
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python :: 3',
    ],
    python_requires='>=3.0',
    keywords='DSSL disease severity score learning',
    packages=find_packages(exclude=['tests']),
    install_requires=['numpy', 'scipy', 'sklearn', 'autograd'],
    # entry_points={
    #     'console_scripts': [
    #         'dsslpy_example=dsslpy.example:main',
    #     ],
    # },
)
